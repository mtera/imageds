# -*- coding: utf-8 -*-
"""
Created on Wed Apr 12 08:27:51 2023

@author: mtera
"""

import os
import cv2
import numpy as np
import glob
import math
import matplotlib.pyplot as plt
import pandas as pd
from PIL import Image
from ImageDS.setting import*
from ImageDS.functions import*


def main(image_folder, fps, n_inj_start, n_inj_fin,
         scale_mm, scale_pix, nozzle_x, nozzle_y):

    # ================== read parameters from setting file ==================
    penet_calmethod, filtering, absorb_th, area_th, absorb_max, ill_max \
        = setting()

    # ================== read images ==================
    # RGB channels
    channels = 3

    # read all images using function called read_images
    imgs, imgs_gray, width, height, data_type, num_images \
        = read_images(image_folder, channels, filtering)

    # read filter image and convert to black and white
    if filtering == 1:
        image_filter_path = image_folder + "/filter.bmp"
        img_filter = cv2.imread(image_filter_path)
        img_filter_gray = cv2.cvtColor(img_filter, cv2.COLOR_RGB2GRAY)
    else:
        img_filter_gray = []

    # average of figures withought spray
    img_ave_background \
        = sum(imgs_gray[0:n_inj_start].astype(np.float64)/n_inj_start)

    # initialize arrays
    csv_out = []
    penet_plt = []
    n_image_plt = []
    time_plt = []
    volume_plt = []
    angle_plt = []

    # ================== start for loop for all figures ==================
    # for n in range(num_images):
    for n in range(num_images):
        print(n)

        n_image_plt = n_image_plt + [n]
        time_plt = time_plt + [n/fps*1000.0]

        # ==== Analyse only in the range of inj start and end =========
        if n >= n_inj_start and n <= n_inj_fin:
            img = imgs[n]
            img_gray = imgs_gray[n]

            # ================== calculate absorbance ==================
            img_absorb, thres \
                = calculate_absorbance(img_ave_background, img_gray, height,
                                       width, ill_max, absorb_max, filtering,
                                       img_filter_gray, absorb_th)

            # ========== create mask  ==========
            mask, img_absorb_colorjet = \
                create_mask(img_absorb, thres, area_th, ill_max,
                            height, width, data_type)

            # ========== calculating volume ==========
            mm_per_pix = scale_mm/scale_pix  # scale mm / pixel
            volume = 0
            for y in range(height):
                for x in range(width):
                    vol = 0
                    if mask[y, x] == 1:
                        vol = \
                            ((((abs(x - nozzle_x) + 0.5)*mm_per_pix)**2)
                             - (((abs(x - nozzle_x) - 0.5)*mm_per_pix)**2)) \
                            * math.pi * mm_per_pix

                        volume = volume + vol

            volume_plt = volume_plt + [volume/2.0]

            # ==========  calculating penetration ==========
            penet_pix = 0

            # calculating maximum y value
            if penet_calmethod == 1:
                for y in range(height):
                    for x in range(width):
                        if mask[y, x] == 1 and penet_pix < y - nozzle_y:
                            penet_pix = y - nozzle_y

            # calculating distance
            if penet_calmethod == 2:
                for y in range(height):
                    for x in range(width):
                        if mask[y, x] == 1:
                            tip_len = math.sqrt((y - nozzle_y)**2
                                                + (x - nozzle_x)**2)
                            if penet_pix < tip_len:
                                penet_pix = tip_len

            penet_plt = penet_plt + [penet_pix * mm_per_pix]

            # ==========  calculating spray angle ==========
            angle = 0
            if 1 in mask:

                s = np.max(np.where(mask[:, nozzle_x] == 1)) - nozzle_y
                half_length_index = int(s/2+nozzle_y)
                width_angle = \
                    np.max(np.where(mask[half_length_index, :] == 1)) \
                    - np.min(np.where(mask[half_length_index, :] == 1))
                angle = np.arctan((width_angle/2) / s/2)*2
                angle = np.rad2deg(angle)

            angle_plt = angle_plt + [angle]

            # ========== save images ==========
            save_images(n, image_folder, img_absorb_colorjet)

    # ==========  output result using function called output ==========
    output(n_image_plt, time_plt, n_inj_start, n_inj_fin,
           penet_plt, angle_plt, num_images, image_folder)
