# -*- coding: utf-8 -*-
"""
Created on Tue Apr 25 08:06:18 2023

@author: mtera
"""
import os
import cv2
import numpy as np
import glob
import math
import matplotlib.pyplot as plt
import pandas as pd
from PIL import Image


# ================== read all images ==================
def read_images(image_folder, channels, filtering):

    '''read all images in the image folder

    Parameters
    ----------
    image_folder : string
        path for the image folder
    channels : integer
        RGB channels
    filtering : integer (1 or 0)
        if use filtering image, 1

    Returns
    -------
    imgs : array
        RGB data array of all images.
    imgs_gray : array
        gray scale array of all images.
    img_filter_gray : array
        gray scale array of filtering image.
    width : integer
        width of images in pixel.
    height : integer
        height of images in pixel.
    data_type : dtype
        data type of images.
    num_images : integer
        total number of images.

    '''
    # obtaining paths for images
    image_path = glob.escape(image_folder)
    image_path = sorted(glob.glob(image_folder + "/*[0-60]*.bmp"))
    image_filter_path = image_folder + "/filter.bmp"

    # obtain the number of images
    num_images = len(image_path)

    # read sample data and obtain image parameters (width, height, data_type)
    sample = Image.open(image_path[0])
    sample_array = np.array(sample)
    width, height = sample.size
    data_type = sample_array.dtype

    # initialize array
    imgs = np.zeros((num_images, height, width, channels), dtype=data_type)
    imgs_gray = np.zeros((num_images, height, width), dtype=data_type)

    # read all images
    for i in range(num_images):
        # RGB
        img = cv2.imread(image_path[i])
        # gray scale
        img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

        imgs[i] = img
        imgs_gray[i] = img_gray

    return imgs, imgs_gray, \
        width, height, data_type, num_images


# ================== calculate absorbance ==================
def calculate_absorbance(img_ave_background, img_gray, height, width,
                         ill_max, absorb_max, filtering, img_filter_gray,
                         absorb_th):

    '''calculate absorbance of gray scale images

    Parameters
    ----------
    img_ave_background : array
        average of gray scale images withough spray.
    img_gray : array
        gray scale images.
    height : integer
        height of images in pixel.
    width : integer
        width of images in pixel.
    ill_max : float
        maximum illumination. normamlly it should be white and 255.
    absorb_max : float
        maximum absorbance
    filtering : integer
        if use filtering image, 1
    img_filter_gray : array
        gray scale filtering image
    absorb_th : float
        threshold of absorbance for calculating penetration.

    Returns
    -------
    img_absorb : array
        absorbance of images
    thres : float
        threshhold of absorbance
    '''

    absorb = img_ave_background/img_gray
    # if masked and black (0 in 255 scale), absorbance should be 0

    for x in range(height):
        for y in range(width):
            if absorb[x, y] < 1:
                absorb[x, y] = 1
            if filtering == 1:
                if img_filter_gray[x, y] == 0:
                    absorb[x, y] = 1

            absorb[x, y] = math.log10(absorb[x, y])

    # if absorbance are below 0, set 0
    absorb[absorb < 0] = 0

    # convert absorbance to 0-255 scale
    img_absorb = absorb * ill_max / absorb_max
    img_absorb[img_absorb > ill_max] = ill_max

    # convert absorbance threshhold to 0-255 scale
    thres = absorb_th * ill_max / absorb_max
    return img_absorb, thres


# ================== create mask ==================
def create_mask(img_absorb, thres, area_th, ill_max,
                height, width, data_type):

    '''create mask for detecting edge of spray

    Parameters
    ----------
    img_absorb : array
        absorbance of images.
    thres : float
        threshhold of absorbance
    area_th : float
        threshold of spray area(noise) for calculating penetration.
    ill_max : float
        maximum illumination. normamlly it should be white and 255.
    height : integer
        height of images in pixel.
    width : integer
        width of images in pixel.
    data_type : dtype
        data type of images.

    Returns
    -------
    mask : array
        mask array which has 1 on edge
    img_absorb_colorjet : array
        absorbance image expressed with color contor
    '''
    # smoothing ing_absorb by using gausian
    img_absorb_gauss = cv2.GaussianBlur(img_absorb, (21, 21), 0)

    # image thresholding
    ret, img_th1 = cv2.threshold(img_absorb_gauss, thres, ill_max, 0)

    # find contours
    contours, hierarchy = cv2.findContours(img_th1.astype(data_type),
                                           cv2.RETR_TREE,
                                           cv2.CHAIN_APPROX_SIMPLE)

    # calculate area
    contours_area = np.array([cv2.contourArea(contours[i])
                              for i in range(len(contours))])

    contours_list = []
    # make a list which counts how many dataset exceed area threshold
    for i in range(len(contours_area)):
        if contours_area[i] > area_th:
            contours_list = contours_list + [i]

    # if there are no dataset which exceed area threshold, still create list
    if len(contours_list) == 0:
        contours_area_max = 0
        contours_area_max_index = 0

        for i in range(len(contours_area)):
            if contours_area[i] > contours_area_max:
                contours_area_max = contours_area[i]
                contours_area_max_index = i
        contours_list = contours_list + [contours_area_max_index]

    mask = np.zeros((height, width))
    for i in contours_list:
        # inside of the contour: 1 / outsie of the contour: 0
        mask = cv2.drawContours(mask, contours, i, 1, -1)

    img_absorb_masked = img_absorb * mask

    # apply clormap
    img_absorb_colorjet = \
        cv2.applyColorMap(img_absorb_masked.astype(data_type),
                          cv2.COLORMAP_JET)

    return mask, img_absorb_colorjet


# ================== save images ==================
def save_images(n, image_folder, img_absorb_colorjet):

    '''save images of absorbance color

    Parameters
    ----------
    n : integer
        number of current loop which also express the number of image.
    image_folder : string
        path for the folder of images
    img_absorb_colorjet : array
        absorbance image expressed with color contor
    img : array

    Returns
    -------
    None.

    '''
    # make folders for saving color contour
    if not os.path.exists(image_folder + "/0absorb"):
        os.mkdir(image_folder + "/0absorb")

    cv2.imwrite(image_folder + "/0absorb/" + '{0:04d}'.format(n)
                + ".jpg", img_absorb_colorjet)


# ================== output result ==================
def output(n_image_plt, time_plt, n_inj_start, n_inj_fin,
           penet_plt, angle_plt, num_images, image_folder):

    '''output resutls to csv and make figure

    Parameters
    ----------
    n_image_plt : list
        list of image number
    time_plt : list
        time for all images
    n_inj_start : integer
        number of the image which start injection
    n_inj_fin : integer
        number of the image which finish injection
    penet_plt : list
        list of penetration for all images
    angle_plt : list
        list of spray angle for all images
    num_images : integer
        number of images
    image_folder : string
        path for the folder of images

    Returns
    -------
    None.

    '''
    # =========== save result in csv ===========
    n_image_plt = ["image number"] + n_image_plt
    time_plt = ["time ms"] + time_plt
    penet_plt = ["liquid penetration mm"] \
        + [0]*n_inj_start + penet_plt + [0]*(num_images-n_inj_fin-1)

    angle_plt = ['spray angle degree'] \
        + [0]*n_inj_start + angle_plt + [0]*(num_images-n_inj_fin-1)

    output = pd.DataFrame({n_image_plt[0]: n_image_plt[1:],
                           time_plt[0]: time_plt[1:],
                           penet_plt[0]: penet_plt[1:],
                           angle_plt[0]: angle_plt[1:]})

    output.to_csv(image_folder + '\\output.csv')

    # =========== make figure ===========
    color = ['#69b3a2', '#3399e6']
    fig, ax = plt.subplots()
    ax2 = ax.twinx()

    lns1 = ax.plot(time_plt[1:n_inj_fin+15], penet_plt[1:n_inj_fin+15],
                   label='liquid penetration', color=color[0])

    lns2 = ax2.plot(time_plt[1:n_inj_fin+15], angle_plt[1:n_inj_fin+15],
                    label='spray angle', color=color[1])

    # add legend
    lns = lns1 + lns2
    labs = [i.get_label() for i in lns]
    ax.legend(lns, labs, loc=1)

    ax.set_xlabel('time ms')
    ax.set_ylabel('liquid penetration mm')
    ax2.set_ylabel('spray agnle deg')
    ax.grid()
    plt.savefig('result.png')

    plt.show()
