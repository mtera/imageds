# -*- coding: utf-8 -*-
"""
Created on Wed Apr 12 08:27:51 2023

@author: mtera
"""


def setting():

    '''
    This is the setting paramters which normally do not need to change.
    When the analysis does not work propery, change those parameters.

    Returns
    -------
    penet_calmethod : integer (1 or 2)
        select the method for calculating penetration
        1: based on y-axis value (default) / 2: based on distance
    filtering : integer (0 or 1)
        0: not use filtering image (default) / 1: using filtering image
    absorb_th : float
        threshold of absorbance for calculating penetration (default: 0.08)
    area_th : float
        threshold of spray area(noise) for calculating penetration\
            (default: 10000)
    absorb_max : integer
         maximum absorbance (default: 0.8)
    ill_max : float
        maximum illumination. normamlly it should be white and 255\
            (default: 255)
    '''

    penet_calmethod = 1
    filtering = 0
    absorb_th = 0.08
    area_th = 10000
    absorb_max = 0.8
    ill_max = 255

    return penet_calmethod, filtering, absorb_th, area_th, absorb_max, ill_max
