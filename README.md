![image1](/uploads/e87fc43cfa0f3dcea3fd7509a2b4bc6f/image1.png)

# Image analysis for Diesel Spray

This package is used for investigating diesel spray images from high-speed camera images. It can create absorbance images and calculate spray angle, spray penegtration and spray volume of liquid phase.

## Installation
For all users it is recommended to install Anaconda. If you have never installed Anaconda, please install from [here](https://www.anaconda.com/download).  
For installing ImageDS package, open anaconda prompt and run this:  
```
pip install --index-url https://test.pypi.org/simple/ ImageDS==2.0.0
```  

memo:  
If you have never installed cv2 module, install it like this:  
```
pip install opencv-python
```

If you have error like 'Could not build wheels for opencv-python which use PEP 517 and cannot be installed directly', try this

```
pip install --upgrade pip setuptools wheel
```
and install opencv-python again with pip.

## Let's practice!
So now you are ready to use this package! There is a sample data and jupyter notebook for practicing. 
Please remenber to change your directory to your repository first.
So open anaconda prompt, and run this:
```
cd your_repository_path
```

and run this:
```
jupyter notebook
```
Now jupyter notebook website should be opened in your browser, and you can find a folder 'howToUse'. Open the folder, and then you can find a notebook 'howToUse.ipynb'. Please run that and follow steps.
you can also find the notebook 'howToUse.ipynb' in gitlab [here](https://gitlab.windenergy.dtu.dk/python-at-risoe/spp-2023/imageds/-/tree/main/howToUse)  

## Package architecture
imageds/  
  - howToUse/  
    - data/  
      - sample spray images for running howToUse  
    - howToUse.ipynb  
  - ImageDS/  
    - functions.py  
	- main.py  
	- setting.py  
  - .gitignore.txt  
  - LICENCE.txt  
  - README.MD  

The architecture of ImageDS function is like below  
![archtecture](/uploads/62a3fe72e9345d09592762cf5e039abc/archtecture.png)

## Peer review
I got a good idea from Bruno in group4 Shaking_Hands that it is better to add sample code and sample data. So I putted a sample code and data inside the main fucntion folder ImageDS.  
Then Jenni suggested me that ImageDS should contain only functions, therefore I make a new folder howToUse and put a notebook and sample data there.  
Pere helped me to update package, and he found out that you have to remove previous wheel files from dist folder before you update a new package. It is wired, but it works..  
Thank you for all you guys!

## git overflow
This project is created and maintained by Motoki, and used git as version controle, but branching and merging function is not used.
