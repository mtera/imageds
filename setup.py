from setuptools import setup
setup(name='ImageDS',
	version='2.0',
	description='ImageDS',
	long_description = 'Image analysis tool for Diesel Spray',
	author='Motoki',
	packages=['ImageDS','howToUse'],
    	install_requires=['opencv-python',
                      'matplotlib',
                      'numpy',
                      'pandas',
                      'Pillow'],
)